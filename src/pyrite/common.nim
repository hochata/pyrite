type
  Matrix*[T] = ref object
    rows, cols: Natural
    data: seq[T]

proc newMatrix*[T](rows, cols: Natural): Matrix[T] =
  Matrix[T](rows: rows, cols: cols, data: newSeq[T](rows*cols))

proc `[]`*(m: Matrix, i, j: Natural): lent Matrix.T =
  m.data[m.cols * i + j]

proc `[]=`*(m: var Matrix, i, j: Natural, val: sink Matrix.T) =
  m.data[m.cols * i + j] = val

template loop*(body: untyped): untyped =
  while true:
    body

template until*(cond: untyped): untyped =
  if cond: break
