# Package

version       = "0.1.0"
author        = "Edgar Quiroz"
description   = "some combinatorial optimization heuristics"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.7.1"

# Tasks

task cleanTests, "remove test executables":
  for testName in listFiles("tests"):
    if testName.split('.').len() == 1:
      rmFile(testName)

#task test, "run tests using balls":
#  exec("balls")
#  cleanTestsTask()

task clean, "deletes generated files":
  rmDir("testresults")
  rmDir("htmldocs")
  rmDir("nimcache")
  cleanTestsTask()

task docs, "generates documentation":
  exec("nim doc --project src/pyrite.nim")
